package com.epam.ta.pageobjects;

import com.epam.ta.factory.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ArticlesPage extends CommonPageObject {
    @FindBy(css = ".evnt-search-filter .evnt-text-fields")
    private WebElement searchField;

    @FindBy(css = ".evnt-article-name")
    private WebElement card;

    @FindBy(css = ".evnt-articles-row .evnt-articles-column")
    private List<WebElement> cards;

    public ArticlesPage(WebDriverFactory webDriverFactory) {
        super(webDriverFactory);
    }

    public void searchFor(String searchTerm) {
        searchField.sendKeys(searchTerm);
    }

    public int getCardCountOnPage() {
        return cards.size();
    }

    public WebElement getCard() {
        return card;
    }
}
