Feature: Search functionality

  Scenario Outline: Search for communities
    Given the communities portal is opened
    When I click the communities button
    And I type "<search-term>" in search
    Then I see the "<card-title>" card
    And I see <count> card
    Examples:
      | search-term | card-title                   | count |
      | Idea Pool   | Idea Pool                    | 1     |
      | Java        | JavaScript Competency Center | 12    |


  Scenario Outline: Search for Sports events
    Given the communities portal is opened
    When I click the events button
    And I type "<search-term>" in search on the events page
    Then I see the "<card-title>" card on the events page
    And I see <count> card on the events page
    Examples:
      | search-term | card-title                   | count |
      | Sport   | IT'Sport                         | 1     |

  Scenario Outline: Search for articles
    Given the communities portal is opened
    When I click the articles button
    And I type "<search-term>" in search at articles page
    Then I see the "<card-title>" card in the articles page
    And I see <count> cards at articles page
    Examples:
      | search-term | card-title                                      | count |
      | Sport       | Public Transport Guide: Katowice                | 8     |
      | Epam        | EPAM Garage Poland: Where business meets fun    | 8     |

  Scenario Outline: Search for speakers
    Given the communities portal is opened
    When I click the speakers button
    And I type "<search-term>" in search at speakers page
    Then I see the "<card-title>" card in the speakers page
    And I see <count> cards at speakers page
    Examples:
      | search-term | card-title        | count |
      | Alex        | Alex Agizim       | 16    |
      | Balazs      | Balazs Cservenak  | 10    |

  Scenario Outline: Search for videos
    Given the communities portal is opened
    When I click the videos button
    And I type "<search-term>" in search at videos page
    Then I see the "<card-title>" card in the videos page
    And I see <count> cards at videos page
    Examples:
      | search-term | card-title        | count |
      | ABAP        | ABAP Environment and AbapGit in SAP Cloud Platform      | 1    |
      | SAP         | DDD - Disappear Devs' Debt                              | 12    |