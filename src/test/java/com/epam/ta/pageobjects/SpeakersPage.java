package com.epam.ta.pageobjects;

import com.epam.ta.factory.WebDriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SpeakersPage extends CommonPageObject {
    @FindBy(css = ".evnt-search-filter .evnt-text-fields")
    private WebElement searchField;

    @FindBy(css = ".evnt-user-name")
    private WebElement card;

    @FindBy(css = ".evnt-users-row .evnt-users-column")
    private List<WebElement> cards;

    public SpeakersPage(WebDriverFactory webDriverFactory) {
        super(webDriverFactory);
    }

    public void searchFor(String searchTerm) {
        searchField.sendKeys(searchTerm);
    }

    public int getCardCountOnPage() {
        return cards.size();
    }

    public WebElement getCard() {
        return card;
    }
}
