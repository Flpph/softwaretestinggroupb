package com.epam.ta.configuration;

import com.epam.ta.factory.WebDriverFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.epam.ta")
public class TestConfiguration {

    @Bean(destroyMethod = "tearDown")
    public WebDriverFactory webDriverFactory() {
        return new WebDriverFactory();
    }
}
