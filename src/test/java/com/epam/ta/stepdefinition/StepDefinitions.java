package com.epam.ta.stepdefinition;

import com.epam.ta.factory.WebDriverFactory;
import com.epam.ta.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class StepDefinitions {

    @Autowired
    private WebDriverFactory webDriverFactory;

    @Autowired
    private MainPage mainPage;

    @Autowired
    private VideosPage videosPage;

    @Autowired
    private CommunitiesPage communitiesPage;

    @Autowired
    private EventsPage eventsPage;

    @Autowired
    private ArticlesPage articlesPage;

    @Autowired
    private SpeakersPage speakersPage;

    @Given("the communities portal is opened")
    public void communitiesPortalOpened() {
        webDriverFactory.getWebDriver().get("https://wearecommunity.io/");
    }

    @When("I click the communities button")
    public void iClickTheCommunitiesButton() {
        mainPage.clickCommunities();
    }

    @When("I click the articles button")
    public void iClickTheArticlesButton() {
        mainPage.clickArticles();
    }

    @When("I click the speakers button")
    public void iClickTheSpeakersButton() {
        mainPage.clickSpeakers();
    }

    @When("I click the videos button")
    public void iClickTheVideosButton(){mainPage.clickVideos();}

    @And("I type {string} in search at videos page")
    public void iTypeInSearchInVideosPage(String searchString){videosPage.searchFor(searchString);}

    @And("I type {string} in search")
    public void iTypeInSearch(String searchString) {
        communitiesPage.searchFor(searchString);
    }

    @And("I type {string} in search at articles page")
    public void iTypeInSearchInArticlesPage(String searchString) {
        articlesPage.searchFor(searchString);
    }

    @And("I type {string} in search at speakers page")
    public void iTypeInSearchInSpeakersPage(String searchString) {
        speakersPage.searchFor(searchString);
    }

    @Then("I see the {string} card")
    public void iSeeTheCard(String title) {
        new WebDriverWait(webDriverFactory.getWebDriver(), Duration.ofSeconds(10))
            .until(ExpectedConditions.textToBePresentInElement(communitiesPage.getCard(), title));
    }

    @Then("I see the {string} card in the videos page")
    public void iSeeTheCardInVideosPage(String title) {
        new WebDriverWait(webDriverFactory.getWebDriver(), Duration.ofSeconds(10))
                .until(ExpectedConditions.textToBePresentInElement(videosPage.getCard(), title));
    }

    @Then("I see the {string} card in the articles page")
    public void iSeeTheCardInArticlesPage(String title) {
        new WebDriverWait(webDriverFactory.getWebDriver(), Duration.ofSeconds(10))
                .until(ExpectedConditions.textToBePresentInElement(articlesPage.getCard(), title));
    }

    @Then("I see the {string} card in the speakers page")
    public void iSeeTheCardInSpeakersPage(String title) {
        new WebDriverWait(webDriverFactory.getWebDriver(), Duration.ofSeconds(10))
                .until(ExpectedConditions.textToBePresentInElement(speakersPage.getCard(), title));
    }

    @And("I see {int} card")
    public void iSeeCard(int count) {
        Assert.assertEquals(count, communitiesPage.getCardcountOnPage());
    }

    @When("I click the events button")
    public void iClickTheEventsButton() {
        mainPage.clickEvents();
    }

    @Then("I see the {string} card on the events page")
    public void iSeeTheCardOnTheEventsPage(String title) {
        new WebDriverWait(webDriverFactory.getWebDriver(), Duration.ofSeconds(10))
                .until(ExpectedConditions.textToBePresentInElement(eventsPage.getCard(), title));
    }

    @And("I type {string} in search on the events page")
    public void iTypeInSearchOnTheEventsPage(String searchString) {
        eventsPage.searchFor(searchString);
    }



    @And("I see {int} card on the events page")
    public void iSeeCountCardOnTheEventsPage(int count) {
        Assert.assertEquals(count, eventsPage.getCardCountOnPage());
    }

    @And("I see {int} cards at videos page")
    public void iSeeCountCardOnTheVideosPage(int count){Assert.assertEquals(count, videosPage.getCardCountOnPage());}

    @And("I see {int} cards at articles page")
    public void iSeeCardsAtArticlesPage(int count) {
        Assert.assertEquals(count, articlesPage.getCardCountOnPage());
    }

    @And("I see {int} cards at speakers page")
    public void iSeeCardsSpeakersPage(int count) {
        Assert.assertEquals(count, speakersPage.getCardCountOnPage());
    }

}
